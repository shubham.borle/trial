#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void member_menus(user_t *u) {
	int choice;
	int book_id;
	do {
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Find Book
                find_book();
				break;
			case 2: // Edit Profile
				edit_profile(u);
				break;
			case 3: // Change Password
				change_password(u);
				break;
			case 4: // Book Availability
				printf("Enter Book id: ");
				scanf("%d", &book_id);
				printf("Copies Available: %d",bookcopy_available(book_id));
				break;
		}
	}while (choice != 0);	
}