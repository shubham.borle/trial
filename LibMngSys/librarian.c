#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void librarian_menus(user_t *u) {
	int choice;
	do {
		printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Add member
                add_member();
				break;
			case 2: // Edit Profile
				edit_profile(u);
				break;
			case 3: // Change Password
				change_password(u);
				break;
			case 4: //Add Book
                add_new_book();
				break;
			case 5: //Find Book
                find_book();
				break;
			case 6:
				break;
			case 7:
				break;
			case 8: //Add Copy
                add_new_copy();
				break;
			case 9:
				break;
			case 10: // Issue Copy
				issue_copy();
				break;
			case 11: // 
				return_copy();
				break;
			case 12:
				break;
			case 13:
				break;
			default:
				printf("Enter valid choice!");
				break;
		}
	}while (choice != 0);		
}

void add_member() {
    // input user details from the librarian.
    user_t m;
    user_accept(&m);
    // write user details into the users file.
    add_user(&m);
}

void add_new_book() {
    // input book details from the librarian.
    book_t b;
    book_accept(&b);
    // write book details into the books file.
    add_book(&b);
}

void find_book() {
	book_t b;
	char temp, bname[20];
	// taking input book name from the librarian.
	printf("Enter book name: ");
	scanf("%c", &temp); // temp statement to clear buffer
	scanf("%[^\n]", bname);

	// find book details into the books file.
	find_book_by_name(bname);
	
}

void add_new_copy() {
    // input bookcopy details from the librarian.
    bookcopy_t bc;
    bookcopy_accept(&bc);
    // write bookcopy details into the bookcopies file.
    add_bookcopy(&bc);
}

void issue_copy() {
	int mid, bid;
	bookcopy_t bc;
	issuerecord_t ir;
	// input member id and book id from the librarian.
	printf("Enter Member id : ");
	scanf("%d", &mid);
	printf("Enter Book id : ");
	scanf("%d", &bid);

	// check copy is available or not
	if (is_bookcopy_available(&bc, bid)) {
		bookcopy_display(&bc);

		printf("Enter issuerecord id : ");
		scanf("%d", &ir.id);

		ir.copyid = bc.id;
		ir.memberid = mid;

		issuerecord_accept(&ir);
		// write issuerecord details into the issuerecords file.
		add_issue_record(&ir);
		// change the available status into the bookcopies file.
		edit_bookcopy_status(bc.id, STATUS_ISSUED);

	} else {
		printf("Copy not available.");
	}	
}

void return_copy() {
	int mid, iid, date_diff;
	bookcopy_t bc;
	// input member id and book id from the librarian.
	printf("Enter Member id : ");
	scanf("%d", &mid);

	// check copy is available or not
	if (list_bookcopy_issued(mid) != 0) {
		printf("Enter Issue Record id : ");
		scanf("%d", &iid);
		
		if (edit_issuerecord_return(iid) == 0)
			printf("you entered wrong issuerecord id.");
	} else {
		printf("No any Book copy issued.");
	}	
}