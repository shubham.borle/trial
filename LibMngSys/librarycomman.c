#include <stdio.h>
#include <string.h>
#include "library.h"


// user functions
void user_accept(user_t *u) {
	printf("id: ");
	scanf("%d", &u->id);
	printf("name: ");
	scanf("%s", u->name);
	printf("email: ");
	scanf("%s", u->email);
	printf("phone: ");
	scanf("%s", u->phone);
	printf("password: ");
	scanf("%s", u->password);
	strcpy(u->role, ROLE_MEMBER);
}

void user_display(user_t *u) {
	printf("%d, %s, %s, %s, %s\n", u->id, u->name, u->email, u->phone, u->role);
}

// book functions
void book_accept(book_t *b) {
    char temp1,temp2,temp3;
	printf("id: ");
	scanf("%d", &b->id);
	printf("name: ");
	scanf("%c", &temp1); // temp1 statement to clear buffer
	scanf("%[^\n]", b->name);
	printf("author: ");
    scanf("%c", &temp2); // temp2 statement to clear buffer
	scanf("%[^\n]", b->author);
	printf("subject: ");
    scanf("%c", &temp3); // temp3 statement to clear buffer
	scanf("%[^\n]", b->subject);
	printf("price: ");
	scanf("%lf", &b->price);
	printf("isbn: ");
	scanf("%s", b->isbn);
}

void book_display(book_t *b) {
	printf("%d, %s, %s, %s, %.2lf, %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}

void add_user(user_t *u) {
	// open the file for appending the data
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}

	// write user data into the file
	fwrite(u, sizeof(user_t), 1, fp);
	printf("user added into file.\n");
	
	// close the file
	fclose(fp);
}

int user_find_by_email(user_t *u, char email[]) {
	FILE *fp;
	int found = 0;
	// open the file for reading the data
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}
	// read all users one by one
	while(fread(u, sizeof(user_t), 1, fp) > 0) {
		// if user email is matching, found 1
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	// close file
	fclose(fp);
	// return
	return found;
}

void add_book(book_t *b) {
	// open the file for appending the data
	FILE *fp;
	fp = fopen(BOOK_DB, "ab");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}

	// write book data into the file
	fwrite(b, sizeof(book_t), 1, fp);
	printf("book added into file.\n");
	
	// close the file
	fclose(fp);
}

void bookcopy_accept(bookcopy_t *bc) {
	char temp;
	printf("id: ");
	scanf("%d", &bc->id);
	printf("book id: ");
	scanf("%d", &bc->bookid);
	printf("rack: ");
	scanf("%d", &bc->rack);
	strcpy(bc->status, STATUS_AVAIL);
}

void bookcopy_display(bookcopy_t *bc) {
	printf("%d, %d, %d, %s\n", bc->id, bc->bookid, bc->rack, bc->status);
}

void add_bookcopy(bookcopy_t *bc) {
	// open the file for appending the data
	FILE *fp;
	fp = fopen(BOOKCOPY_DB, "ab");
	if(fp == NULL) {
		perror("failed to open bookcopies file");
		return;
	}

	// write bookcopy data into the file
	fwrite(bc, sizeof(bookcopy_t), 1, fp);
	printf("bookcopy added into file.\n");
	
	// close the file
	fclose(fp);
}

void find_book_by_name(char book[]) {
	FILE *fp;
	int found = 0;
	book_t b;
	// open the file for reading the data
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		// if book name is matching, found 1
		if(strstr(b.name, book) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}

int is_bookcopy_available(bookcopy_t *bc, int bid) {
	FILE *fp;
	int found = 0;
	// open the file for reading the data
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("failed to open bookcopies file");
		return found;
	}
	// read all bookcopies one by one
	while(fread(bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if bookcopy status is available, found 1
		if(strcmp(bc->status, STATUS_AVAIL) == 0) {
			found = 1;
			break;
		}
	}
	// close file
	fclose(fp);
	// return
	return found;
}

void add_issue_record(issuerecord_t *ir) {
	// open the file for appending the data
	FILE *fp;
	fp = fopen(ISSUERECORD_DB, "ab");
	if(fp == NULL) {
		perror("failed to open issuerecords file");
		return;
	}

	// write issuerecords data into the file
	fwrite(ir, sizeof(issuerecord_t), 1, fp);
	printf("issuerecord added into file.\n");
	
	// close the file
	fclose(fp);
}

// issuerecord functions
void issuerecord_accept(issuerecord_t *ir) {
	// printf("id: ");
	// scanf("%d", &ir->id);
	// printf("copy id: ");
	// scanf("%d", &ir->copyid);
	// printf("member id: ");
	// scanf("%d", &ir->memberid);
	// printf("issue ");
	ir->issue_date = date_current();
	ir->return_duedate = date_add(ir->issue_date, BOOK_RETURN_DAYS);
	memset(&ir->return_date, 0, sizeof(date_t));
	ir->fine_amount = 0.0;
}

void issuerecord_display(issuerecord_t *ir) {
	printf("issue record: %d, copy: %d, member: %d, find: %.2lf\n", ir->id, ir->copyid, ir->memberid, ir->fine_amount);
	printf("issue ");
	date_print(&ir->issue_date);
	printf("return due ");
	date_print(&ir->return_duedate);
	printf("return ");
	date_print(&ir->return_date);
}

void edit_bookcopy_status(int bcid, char status[]) {
	int found = 0;
	FILE *fp;
	bookcopy_t c;
	// open bookcopies file
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open bookcopies file");
		return;
	}
	// read bookcopies one by one and check if bookcopy with provided copyid is found.
	while(fread(&c, sizeof(bookcopy_t), 1, fp) > 0) {
		if(bcid == c.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		long size = sizeof(bookcopy_t);
		strcpy(c.status, status);
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite bookcopy details into the file
		fwrite(&c, size, 1, fp);
		bookcopy_display(&c);
		printf("bookcopy updated.\n");
	}
	else // if not found
		// show message to user that bookcopy not found.
		printf("Bookcopy not found.\n");
	// close books file
	fclose(fp);
}

int bookcopy_available(int bid) {
	FILE *fp;
	int found = 0;
	bookcopy_t bc;
	// open the file for reading the data
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("failed to open bookcopies file");
		return found;
	}
	// read all bookcopies one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if bookcopy status is available, found 1
		if(bc.bookid == bid && strcmp(bc.status, STATUS_AVAIL) == 0) {
			found++;
		}
	}
	// close file
	fclose(fp);
	// return
	return found;
}

void edit_profile(user_t *u) {
	FILE *fp;
	user_t temp;
	// open users file
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open users file");
		return;
	}
	// read users one by one and check if user with provided user is found.
	while(fread(&temp, sizeof(user_t), 1, fp) > 0) {
		if(u->id == temp.id) {
			long size = sizeof(user_t);
			printf("name (%s): ", u->name);
			scanf("%s", temp.name);
			printf("email (%s): ", u->email);
			scanf("%s", temp.email);
			printf("phone (%s): ", u->phone);
			scanf("%s", temp.phone);
			// take file position one record behind.
			fseek(fp, -size, SEEK_CUR);
			// overwrite bookcopy details into the file
			fwrite(&temp, size, 1, fp);
			printf("Your updated profile info ");
			user_display(&temp);
			printf("user updated.");
			break;
		}
	}
	// close users file
	fclose(fp);	
}

void change_password(user_t *u) {
	FILE *fp;
	user_t temp;
	char cur_pass[10], new_pass[10], conf_pass[10]; 
	int change = 0;
	do {
		printf("Enter your current password (or enter 0 to cancel): ");
		scanf("%s", cur_pass);
		if (!strcmp(cur_pass, u->password)) {
			do {
				printf("Enter New password: ");
				scanf("%s", new_pass);
				printf("Confirm password: ");
				scanf("%s", conf_pass);
				if (strcmp(new_pass, conf_pass) == 0) {
					change = 1;
				} else {
					printf("not matching! please enter it again!\n");
				}
			} while (strcmp(new_pass, conf_pass) != 0);
			
			if (change) {
				// open users file
				fp = fopen(USER_DB, "rb+");
				if(fp == NULL) {
					perror("cannot open users file");
					return;
				}
				// read users one by one and check if user with provided user is found.
				while(fread(&temp, sizeof(user_t), 1, fp) > 0) {
					if(u->id == temp.id) {
						long size = sizeof(user_t);
						//copying new password in user
						strcpy(temp.password, new_pass);
						// take file position one record behind.
						fseek(fp, -size, SEEK_CUR);
						// overwrite user password into the file
						fwrite(&temp, size, 1, fp);
						strcpy(u->password, new_pass);
						strcpy(cur_pass, "0");
						printf("password changed.");
						break;
					}
				}
				// close users file
				fclose(fp);
			}
				
		} else {
			printf("Your current password not matching\n");
		}
	} while (strcmp(cur_pass, "0") != 0);
}

int list_bookcopy_issued(int mid) {
	FILE *fp;
	int found = 0;
	issuerecord_t ir;
	// open the issuerecords file for reading the data
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL) {
		perror("failed to open issuerecords file");
		return found;
	}
	// read all issuerecords one by one
	while(fread(&ir, sizeof(issuerecord_t), 1, fp) > 0) {
		// if bookcopy status is available, found 1
		if(ir.memberid == mid) {
			issuerecord_display(&ir);
			found = 1;
			break;
		}
	}
	// close file
	fclose(fp);
	// return
	return found;
}

int edit_issuerecord_return(int iid) {
	FILE *fp;
	issuerecord_t temp;
	int date_diff, found = 0;
	// open issuerecords file
	fp = fopen(ISSUERECORD_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open issuerecords file");
		return 0;
	}
	// read issuerecords one by one and check if issuerecord_id found.
	while(fread(&temp, sizeof(issuerecord_t), 1, fp) > 0) {
		if(iid == temp.id) {
			found = 1;
			long size = sizeof(issuerecord_t);
			printf("Enter return ");
			date_accept(&temp.return_date);
			date_diff = date_cmp(temp.return_date, temp.return_duedate);
			// update fine amount if any
			if(date_diff > 0)
				temp.fine_amount = date_diff * FINE_PER_DAY;

			// take file position one record behind.
			fseek(fp, -size, SEEK_CUR);
			// overwrite bookcopy details into the file
			fwrite(&temp, size, 1, fp);
			issuerecord_display(&temp);
			edit_bookcopy_status(temp.copyid, STATUS_AVAIL);
			printf("issuerecord updated");
			break;
		}
	}
	// close users file
	fclose(fp);	
	return found;
}