#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_menus(user_t *u) {
	int choice;
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees Report\n5. Fine Report\n6. Book By Categories/Subjects\n7. Book Availability\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Appoint Librarian
				appoint_librarian();
				break;
			case 2: // Edit Profile
				edit_profile(u);
				break;
			case 3: // Change Password
				change_password(u);
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
            case 7:
                break;
		}
	}while (choice != 0);	
}

void appoint_librarian() {
	// input librarian details
	user_t u;
	user_accept(&u);
	// change user role to librarian
	strcpy(u.role, ROLE_LIBRARIAN);
	// add librarian into the users file
	add_user(&u);
}